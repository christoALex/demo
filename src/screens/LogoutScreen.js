import React, { useState } from 'react';
import { Alert } from 'react-native';
import { useDispatch } from 'react-redux';
import * as authActions from '../store/actions/auth';
import { useSelector } from 'react-redux';
import { useFocusEffect } from '@react-navigation/native';
import Loading from '../components/Loading';

const LogoutScreen = ({ navigation }) => {
	const [loader, setLoader] = useState(false);
	const dispatch = useDispatch();
	let userData = useSelector((state) => state.Auth);
	let playerId = useSelector(
		(state) => state.Notification.notificationData.userId,
	);

	const loginOut = async () => {
		setLoader(true);
		navigation.navigate('HomeStack');
		await dispatch(authActions.logout(userData.accessToken, playerId));
		setLoader(false);
	};

	useFocusEffect(
		React.useCallback(() => {
			Alert.alert(
				'',
				'Are you sure you want to logout ?',
				[
					{
						text: 'Cancel',
						onPress: () => navigation.goBack(),
						style: 'cancel',
					},
					{ text: 'Logout', onPress: () => loginOut() },
				],
				{ cancelable: false },
			);
		}, [navigation]),
	);

	return loader ? <Loading /> : null;
};
export default LogoutScreen;
