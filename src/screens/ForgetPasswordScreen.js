import React, { useState } from 'react';
import {
	StyleSheet,
	Text,
	View,
	TextInput,
	TouchableOpacity,
	Image,
} from 'react-native';
import { useSelector } from 'react-redux';
import axios from 'axios';
import { API } from '../utils/config';
import Btn from 'react-native-micro-animated-button';

const ForgetPasswordScreen = ({ navigation }) => {
	const [msg, setMsg] = useState('');
	const [email, setEmail] = useState('');
	const [color, setColor] = useState('green');
	let userData = useSelector((state) => state.Auth);
	let access_token = userData.accessToken;

	const resetPassword = async () => {
		let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
		if (reg.test(email) === false) {
			setColor('red');
			setMsg('Invalid Email');
			btn.reset();
			return false;
		}

		try {
			let response = await axios.post(
				`${API}/auth/reset-password`,
				{
					email: email,
				},
				{
					headers: {
						Authorization: `Bearer ${access_token}`,
					},
				},
			);
			setColor('green');
			setMsg(response.data);
			btn.reset();
		} catch (error) {
			btn.reset();
		}
	};

	return (
		<View style={styles.container}>
			<Image
				style={{ width: 200, height: 160, marginBottom: 15 }}
				source={require('../images/logo.png')}
			/>
			<Text style={{ fontWeight: 'bold', fontSize: 20, marginTop: 20 }}>
				Forgot Password ?
			</Text>
			<Text style={styles.textGray}>
				Please enter your email address to request a new
			</Text>
			<Text style={styles.textGray}>password reset</Text>
			<TextInput
				style={styles.textInput}
				placeholder='Email id'
				value={email}
				onChangeText={(text) => setEmail(text.trim())}
			/>
			<View style={[{ width: '80%' }]}>
				<Btn
					label='Submit'
					backgroundColor='#4682b4'
					labelStyle={{ color: 'white' }}
					foregroundColor='white'
					onPress={() => resetPassword()}
					ref={(ref) => (btn = ref)}
					style={[{ borderRadius: 5, alignSelf: 'center', marginTop: 30 }]}
				/>
				<Text style={{ alignSelf: 'center', color: color }}>{msg}</Text>
			</View>
			<TouchableOpacity
				style={{ marginTop: 10 }}
				onPress={() => navigation.goBack()}
			>
				<Text style={{ color: 'red', textDecorationLine: 'underline' }}>
					Cancel
				</Text>
			</TouchableOpacity>
		</View>
	);
};

export default ForgetPasswordScreen;

const styles = StyleSheet.create({
	container: {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center',
		backgroundColor: '#fff',
	},
	textGray: {
		color: 'gray',
		padding: 2,
		fontSize: 12,
	},
	textInput: {
		height: 40,
		width: '80%',
		borderColor: 'gray',
		borderWidth: 1,
		marginTop: 15,
		padding: 10,
	},
});
