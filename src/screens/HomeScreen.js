import React, { useEffect, useState } from "react";
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  FlatList,
} from "react-native";
import { CachedImage, ImageCacheProvider } from "react-native-cached-image";
import { useSelector, useDispatch } from "react-redux";
import OneSignal from "react-native-onesignal";
import * as notificationActions from "../store/actions/notification";
import Loading from "../components/Loading";
import moment from "moment";
import axios from "axios";
import { API } from "../utils/config";

const HomeScreen = ({ navigation }) => {
  const [loader, setLoader] = useState(false);
  const [data, setData] = useState([]);
  let userData = useSelector((state) => state.Auth.userData);
  const { name } = userData.user;
  const { access_token } = userData;
  let admin = name == "admin" ? true : false;
  const dispatch = useDispatch();
  const [page, setPage] = useState(1);
  const [refreshing, setRefreshing] = useState(false);

  useEffect(() => {
    if (admin) {
      fetchMyNotification();
    }
  }, [page]);

  const fetchMyNotification = async () => {
    try {
      setRefreshing(true);
      const response = await axios.get(
        `${API}/notification?page=${page}&limit=15`,
        {
          headers: {
            Authorization: `Bearer ${access_token}`,
          },
        },
      );
      const list = response.data;
      if (response.status === 200) {
        page == 1 ? setData(list) : setData((data) => [...data, ...list]);
      }
      setRefreshing(false);
    } catch (error) {
      if (error.response.data.statusCode == 401) {
        Toast.show("Access Expired ! Please Login Again");
        setTimeout(function () {
          dispatch(logout(access_token));
        }, 1000);
      }
      let message;
      if (error.response !== undefined) {
        throw error.response.data;
      } else {
        message = error;
      }
      throw message;
    }
    setRefreshing(false);
  };

  const onIds = async (device) => {
    await dispatch(notificationActions.addNotification(access_token, device));
  };

  useEffect(() => {
    OneSignal.addEventListener("ids", onIds);
    return () => {
      OneSignal.removeEventListener("received", onIds);
    };
  });

  const ItemSeparatorView = () => {
    return (
      <View
        style={{
          height: 0.5,
          width: "100%",
          backgroundColor: "#C8C8C8",
        }}
      />
    );
  };

  const ListHeader = () => {
    return (
      <View style={styles.headerFooterStyle}>
        <Text style={styles.textStyle}>Notifications</Text>
      </View>
    );
  };

  const ItemView = ({ item }) => {
    let formatDate = moment(new Date(item.createdAt)).format(
      "DD/MM/YYYY HH:MM:ss",
    );
    return (
      <View>
        <View style={{ flexDirection: "row", justifyContent: "space-between" }}>
          <Text style={styles.itemStyle}>{item.title}</Text>
          <Text style={{ padding: 12, fontSize: 12 }}>{formatDate}</Text>
        </View>
        <Text style={{ padding: 12, fontSize: 12 }}>{item.body}</Text>
      </View>
    );
  };

  const onScrollHandler = async () => {
    setPage(page + 1);
  };

  return admin ? (
    loader ? (
      <Loading />
    ) : (
      <View style={styles.container}>
        <FlatList
          data={data}
          keyExtractor={(item, index) => index.toString()}
          ItemSeparatorComponent={ItemSeparatorView}
          ListHeaderComponent={ListHeader}
          renderItem={ItemView}
          showsVerticalScrollIndicator={false}
          onRefresh={() => setPage(1)}
          refreshing={refreshing}
          onEndReached={() => onScrollHandler()}
          onEndReachedThreshold={0.5}
        />
      </View>
    )
  ) : (
    <View style={styles.container}>
      <ImageCacheProvider>
        <CachedImage
          style={{ width: 500, height: "115%" }}
          source={require("../images/grd.png")}
          resizeMode={"cover"}
        />
      </ImageCacheProvider>

      <TouchableOpacity
        hitSlop={{ top: 20, bottom: 20, left: 50, right: 50 }}
        style={{
          backgroundColor: "transparent",
          height: 48,
          width: 50,
          position: "absolute",
          top: "25%",
          bottom: 0,
          left: "48%",
          right: 0,
        }}
        onPress={() =>
          navigation.navigate("Grid", {
            itemId: "Grid",
          })
        }
      />
      <TouchableOpacity
        hitSlop={{ top: 50, bottom: 20, left: 50, right: 50 }}
        style={{
          backgroundColor: "transparent",
          height: 48,
          width: 50,
          position: "absolute",
          top: "40%",
          bottom: 0,
          left: "75%",
          right: 0,
        }}
        onPress={() =>
          navigation.navigate("Battery", {
            itemId: "battery",
          })
        }
      />
      <TouchableOpacity
        hitSlop={{ top: 20, bottom: 20, left: 50, right: 50 }}
        style={{
          backgroundColor: "transparent",
          height: 48,
          width: 50,
          position: "absolute",
          top: "70%",
          bottom: 0,
          left: "60%",
          right: 0,
        }}
        onPress={() =>
          navigation.navigate("Home Demand", {
            itemId: "Home Demand",
          })
        }
      />
      <TouchableOpacity
        hitSlop={{ top: 40, bottom: 20, left: 50, right: 50 }}
        style={{
          backgroundColor: "transparent",
          height: 48,
          width: 50,
          position: "absolute",
          top: "40%",
          bottom: 0,
          left: "18%",
          right: 0,
        }}
        onPress={() =>
          navigation.navigate("Electric Vehicle", {
            itemId: "Electric Vehicle",
          })
        }
      />
      <TouchableOpacity
        hitSlop={{ top: 20, bottom: 20, left: 50, right: 50 }}
        style={{
          backgroundColor: "transparent",
          height: 48,
          width: 50,
          position: "absolute",
          top: "72%",
          bottom: 0,
          left: "27%",
          right: 0,
        }}
        onPress={() =>
          navigation.navigate("PV Solar", {
            itemId: "solar",
          })
        }
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#ffffff",
  },
  emptyListStyle: {
    padding: 10,
    fontSize: 18,
    textAlign: "center",
  },
  itemStyle: {
    padding: 10,
    fontWeight: "bold",
  },
  headerFooterStyle: {
    width: "100%",
    height: 75,
    backgroundColor: "#4682b4",
  },
  textStyle: {
    textAlign: "center",
    color: "#fff",
    fontSize: 18,
    padding: 7,
  },
});

export default HomeScreen;
