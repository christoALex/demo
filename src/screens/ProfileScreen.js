import React, { useState } from 'react';
import {
	View,
	Text,
	StyleSheet,
	TextInput,
	Image,
	TouchableOpacity,
	Modal,
	Dimensions,
} from 'react-native';
import { useSelector } from 'react-redux';
const deviceWidth = Dimensions.get('window').width;
import { Card } from 'react-native-elements';
import Btn from 'react-native-micro-animated-button';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import axios from 'axios';
import { API } from '../utils/config';
import Toast from 'react-native-simple-toast';

const ProfileScreen = () => {
	let userData = useSelector((state) => state.Auth.userData);
	const { name, email } = userData.user;
	let access_token = userData.access_token;
	const [showModal, setShowModal] = useState(false);
	const [oldPassword, setOldPassword] = useState('');
	const [newPassword, setNewPassword] = useState('');
	const [confirmPassword, setConfirmPassword] = useState('');
	const [message, setMessage] = useState('');
	const [msgColor, setMsgColor] = useState('red');
	let btn;
	const changePasswordHandler = async () => {
		if (oldPassword == '' && newPassword == '' && confirmPassword == '') {
			btn.reset();
			setMessage('Password required');
			return;
		} else if (newPassword == '') {
			btn.reset();
			setMessage('New password required');
			return;
		} else if (confirmPassword == '') {
			btn.reset();
			setMessage('Confirm password required');
			return;
		} else if (oldPassword && confirmPassword !== newPassword) {
			btn.reset();
			setMessage('Password mismatching');
			return;
		}

		try {
			let response = await axios.put(
				`${API}/auth/change-password`,
				{
					oldPassword: oldPassword,
					newPassword: newPassword,
					confirmNewPassword: confirmPassword,
				},
				{
					headers: {
						Authorization: `Bearer ${access_token}`,
					},
				},
			);
			btn.reset();
			setMessage(response.data);
			if (response.data.includes('Successfully')) {
				setMsgColor('green');
				setTimeout(function () {
					setShowModal(false);
					setOldPassword('');
					setNewPassword('');
					setConfirmPassword('');
				}, 1000);
			} else {
				setMsgColor('red');
			}
		} catch (error) {
			if (error.response.data.statusCode == 401) {
				Toast.show('Access Expired ! Please Login Again');
			}
			btn.reset();
		}
	};

	return (
		<View style={styles.container}>
			<View style={styles.profileImageStyle}>
				<Image
					style={{ height: 100, width: 100 }}
					source={require('../images/user.png')}
				/>
			</View>
			<View style={{ margin: 50 }}>
				<TextInput
					style={styles.textStyle}
					placeholder='First Name'
					value={name}
					editable={false}
				/>
				<TextInput
					style={styles.textStyle}
					placeholder='E-mail'
					value={email}
					editable={false}
				/>
			</View>
			<TouchableOpacity
				style={{ width: '80%', alignSelf: 'center' }}
				onPress={() => setShowModal(true)}
			>
				<View style={styles.changePasswordButton}>
					<Text>Change Password</Text>
					<Icon name='chevron-right' size={24} />
				</View>
			</TouchableOpacity>
			<Modal
				transparent={true}
				animationType={'fade'}
				visible={showModal}
				onRequestClose={() => {
					setShowModal(false);
				}}
			>
				<View
					style={{
						width: '100%',
						height: '100%',
						justifyContent: 'center',
						backgroundColor: 'rgba(100,100,100,0.5)',
						padding: deviceWidth / 20,
					}}
				>
					<Card title='Change Password' borderRadius={20}>
						<TouchableOpacity
							hitSlop={{ top: 50, bottom: 50, left: 50, right: 50 }}
							onPress={() => setShowModal(false)}
							style={styles.closeButton}
						>
							<Icon name='close' size={25} color='#4682b4' />
						</TouchableOpacity>
						<TextInput
							style={styles.textInput}
							placeholder='Current Password'
							secureTextEntry={true}
							value={oldPassword}
							placeholderTextColor='gray'
							onChangeText={(text) => setOldPassword(text)}
						/>
						<TextInput
							style={styles.textInput}
							placeholder='New Password'
							secureTextEntry={true}
							value={newPassword}
							placeholderTextColor='gray'
							onChangeText={(text) => setNewPassword(text)}
						/>
						<TextInput
							style={styles.textInput}
							placeholder='Confirm Password'
							secureTextEntry={true}
							value={confirmPassword}
							placeholderTextColor='gray'
							onChangeText={(text) => setConfirmPassword(text)}
						/>
						<Btn
							label='Submit'
							backgroundColor='#4682b4'
							labelStyle={{ color: 'white' }}
							foregroundColor='white'
							onPress={() => changePasswordHandler()}
							ref={(ref) => (btn = ref)}
							style={[{ borderRadius: 5, alignSelf: 'center', marginTop: 30 }]}
						/>
						<Text style={{ alignSelf: 'center', color: msgColor }}>
							{message}
						</Text>
					</Card>
				</View>
			</Modal>
		</View>
	);
};

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: '#FFF',
	},
	textStyle: {
		borderLeftColor: '#FFF',
		borderRightColor: '#FFF',
		borderTopColor: '#FFF',
		borderBottomColor: 'gray',
		borderWidth: 1,
		marginTop: 20,
		color: 'black',
		padding: 10,
	},
	profileImageStyle: {
		margin: 10,
		justifyContent: 'center',
		alignItems: 'center',
	},
	changePasswordButton: {
		flexDirection: 'row',
		justifyContent: 'space-between',
		height: 65,
		alignItems: 'center',
		borderRadius: 10,
		paddingHorizontal: 10,
		elevation: 0,
		borderWidth: 1,
		borderColor: '#D6D6D6',
	},
	modal: {
		flex: 1,
		alignItems: 'center',
		backgroundColor: '#f7021a',
		padding: 100,
	},
	text: {
		color: '#3f2949',
		marginTop: 10,
	},
	textInput: {
		height: 40,
		//width: '80%',
		borderColor: 'gray',
		borderWidth: 1,
		margin: 5,
		padding: 10,
	},
	closeButton: {
		position: 'absolute',
		right: 5,
	},
});

export default ProfileScreen;
