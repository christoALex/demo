import React, { useCallback, useState } from 'react';
import { View, StyleSheet } from 'react-native';
import NavigatorButtons from '../../components/NavigatorButtons';
import ThreeTileDashboard from '../../components/ThreeTileDashboard';
import { useSelector, useDispatch } from 'react-redux';
import * as dataActions from '../../store/actions/data';
import { useFocusEffect } from '@react-navigation/native';
import Loading from '../../components/Loading';

const Battery = ({ navigation }) => {
	let gridData = useSelector((state) => state.GridData.resGridData[0]) || [];
	let access_token = useSelector((state) => state.Auth.userData.access_token);
	const [loader, setLoader] = useState(false);
	const dispatch = useDispatch();

	const getData = useCallback(async () => {
		try {
			setLoader(true);
			await dispatch(dataActions.getData(access_token));
			setLoader(false);
		} catch (error) {
			setLoader(false);
		}
	}, [dispatch]);

	useFocusEffect(
		React.useCallback(() => {
			getData();
		}, [dispatch]),
	);

	return loader ? (
		<Loading />
	) : (
		<View style={styles.container}>
			<ThreeTileDashboard
				header1='State of Charge'
				header2='Charge Power'
				header3='Discharge Power'
				value1={`${gridData['battery_state_of_charge_soc']} %`}
				value2={`${gridData['battery_charge_power']} W`}
				value3={`${gridData['battery_discharge_power']} W`}
			/>
			<NavigatorButtons navigation={navigation} page='Battery' />
		</View>
	);
};

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: '#fff',
	},
});

export default Battery;
