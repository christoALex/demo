import React, { useCallback, useState } from 'react';
import { View, StyleSheet } from 'react-native';
import NavigatorButtons from '../../components/NavigatorButtons';
import ThreeTileDashboard from '../../components/ThreeTileDashboard';
import { useSelector, useDispatch } from 'react-redux';
import * as dataActions from '../../store/actions/data';
import { useFocusEffect } from '@react-navigation/native';
import Loading from '../../components/Loading';

const Ev = ({ navigation }) => {
	let gridData = useSelector((state) => state.GridData.resGridData[0]) || [];
	let access_token = useSelector((state) => state.Auth.userData.access_token);
	const [loader, setLoader] = useState(false);
	const dispatch = useDispatch();

	const getData = useCallback(async () => {
		try {
			setLoader(true);
			await dispatch(dataActions.getData(access_token));
			setLoader(false);
		} catch (error) {
			setLoader(false);
		}
	}, [dispatch]);

	useFocusEffect(
		React.useCallback(() => {
			getData();
		}, [dispatch]),
	);

	return loader ? (
		<Loading />
	) : (
		<View style={styles.container}>
			<ThreeTileDashboard
				header1='Charging Energy'
				header2='Charging Power'
				header3='Number of Charges'
				value1={`${gridData['ev_charging_energy']} kWh`}
				value2={`${gridData['ev_charging_power']} W`}
				value3={`${gridData['number_of_charges__cycles']}`}
			/>
			<NavigatorButtons navigation={navigation} page='Electric Vehicle' />
		</View>
	);
};

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: '#fff',
	},
});

export default Ev;
