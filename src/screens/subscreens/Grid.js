import React, { useEffect, useCallback, useState } from 'react';
import { View, StyleSheet } from 'react-native';
import NavigatorButtons from '../../components/NavigatorButtons';
import FourTileDashboard from '../../components/FourTileDashboard';
import { useSelector, useDispatch } from 'react-redux';
import * as dataActions from '../../store/actions/data';
import { useFocusEffect } from '@react-navigation/native';
import Loading from '../../components/Loading';

const Grid = ({ navigation }) => {
	let gridData = useSelector((state) => state.GridData.resGridData[0]) || [];
	let access_token = useSelector((state) => state.Auth.userData.access_token);
	const [loader, setLoader] = useState(false);

	const dispatch = useDispatch();

	const getData = useCallback(async () => {
		try {
			setLoader(true);
			await dispatch(dataActions.getData(access_token));
			setLoader(false);
		} catch (error) {
			setLoader(false);
		}
	}, [dispatch]);

	useFocusEffect(
		React.useCallback(() => {
			getData();
		}, [dispatch]),
	);

	useEffect(() => {
		getData();
	}, []);

	return loader ? (
		<Loading />
	) : (
		<View style={styles.container}>
			<FourTileDashboard
				header1={'Import Active\nPower'}
				header2={'Export Active\nPower'}
				header3={'Import Energy\nto date'}
				header4={'Export Energy\nto date'}
				value1={`${gridData['grid_imported_active_power']} W`}
				value2={`${gridData['grid_exported_active_power']} W`}
				value3={`${parseFloat(gridData['grid_imported_energy_to_date']).toFixed(
					2,
				)} kWh`}
				value4={`${parseFloat(gridData['grid_exported_energy_to_date']).toFixed(
					2,
				)} kWh`}
				icon={true}
			/>
			<NavigatorButtons navigation={navigation} page='Grid' />
		</View>
	);
};

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: '#ffffff',
	},
});

export default Grid;
