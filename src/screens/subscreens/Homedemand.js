import React, { useCallback, useEffect, useState } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import Loading from '../../components/Loading';
import { AnimatedCircularProgress } from 'react-native-circular-progress';
import NavigatorButtons from '../../components/NavigatorButtons';

import { useSelector, useDispatch } from 'react-redux';
import * as dataActions from '../../store/actions/data';
import { useFocusEffect } from '@react-navigation/native';

const Homedemand = ({ navigation }) => {
	let gridData = useSelector((state) => state.GridData.resGridData[0]) || [];
	const [loader, setLoader] = useState(false);
	let access_token = useSelector((state) => state.Auth.userData.access_token);

	const dispatch = useDispatch();

	const getData = useCallback(async () => {
		try {
			setLoader(true);
			await dispatch(dataActions.getData(access_token));
			setLoader(false);
		} catch (error) {
			setLoader(false);
		}
	}, [dispatch]);

	useEffect(() => {
		getData();
	}, []);

	useFocusEffect(
		React.useCallback(() => {
			getData();
		}, [dispatch]),
	);

	return loader ? (
		<Loading />
	) : (
		<View
			style={{
				flex: 1,
				alignItems: 'center',
				backgroundColor: '#FFF',
			}}
		>
			<View
				style={{
					alignSelf: 'flex-start',
					marginBottom: '5%',
					marginLeft: 30,
					marginTop: 20,
				}}
			></View>
			<AnimatedCircularProgress
				size={200}
				width={15}
				fill={82}
				tintColor='#4682b4'
				rotation={0}
				lineCap='round'
				backgroundColor='#EDEDED'
			>
				{(fill) => (
					<View style={{ alignItems: 'center' }}>
						<Text
							style={{
								fontSize: 28,
								color: '#4682b4',
								fontWeight: 'bold',
								textAlign: 'center',
							}}
						>
							{`${gridData['washing_machine_home_power_demand']} W`}
						</Text>
						<Text style={{ fontWeight: 'bold', color: '#6d6875' }}>
							Home Power Demand
						</Text>
					</View>
				)}
			</AnimatedCircularProgress>
			<NavigatorButtons navigation={navigation} page='Home Demand' />
		</View>
	);
};

const styles = StyleSheet.create({});

export default Homedemand;
