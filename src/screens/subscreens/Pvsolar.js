import React, { useCallback, useState } from 'react';
import { View, StyleSheet } from 'react-native';
import NavigatorButtons from '../../components/NavigatorButtons';
import FourTileDashboard from '../../components/FourTileDashboard';
import { useSelector, useDispatch } from 'react-redux';
import * as dataActions from '../../store/actions/data';
import { useFocusEffect } from '@react-navigation/native';
import Loading from '../../components/Loading';

const Pvsolar = (props) => {
	let gridData = useSelector((state) => state.GridData.resGridData[0]) || [];
	let access_token = useSelector((state) => state.Auth.userData.access_token);
	const [loader, setLoader] = useState(false);
	const dispatch = useDispatch();

	const getData = useCallback(async () => {
		try {
			setLoader(true);
			await dispatch(dataActions.getData(access_token));
			setLoader(false);
		} catch (error) {
			setLoader(false);
		}
	}, [dispatch]);

	useFocusEffect(
		React.useCallback(() => {
			getData();
		}, [dispatch]),
	);

	return loader ? (
		<Loading />
	) : (
		<View style={styles.container}>
			<FourTileDashboard
				header1='Generated Power'
				header2='Energy Today'
				header3='Weekly Energy'
				header4='Energy to date'
				value1={`${gridData['pv_generated_power']} W`}
				value2={`${gridData['pv_energy_today']} kWh`}
				value3={`${gridData['pv_energy_this_week']} kWh`}
				value4={`${gridData['pv_energy_to_date']} kWh`}
				icon={false}
			/>
			<NavigatorButtons navigation={props.navigation} page='PV Solar' />
		</View>
	);
};

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: '#fff',
	},
});

export default Pvsolar;
