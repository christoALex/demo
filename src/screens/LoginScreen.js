import React, { useState, useEffect } from 'react';
import {
	View,
	Text,
	StyleSheet,
	TextInput,
	Image,
	TouchableOpacity,
	Keyboard,
	TouchableWithoutFeedback,
	KeyboardAvoidingView,
} from 'react-native';

import { useDispatch } from 'react-redux';
import * as authActions from '../store/actions/auth';
import Btn from 'react-native-micro-animated-button';
import { useSelector } from 'react-redux';

const LoginScreen = ({ navigation }) => {
	const [username, setUserName] = useState('');
	const [password, setPassword] = useState('');
	const [error, setError] = useState('');
	const [isKeyboardVisible, setKeyboardVisible] = useState(false);
	let userData = useSelector((state) => state.Auth);
	let access_token = userData.accessToken;

	const dispatch = useDispatch();
	let btn = '';
	const loginHandler = async () => {
		let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
		if (username == '') {
			btn.reset();
			setError('Enter valid credentials');
			return;
		} else if (reg.test(username) === false) {
			btn.reset();
			setError('Invalid Email');
			return;
		} else if (password == '') {
			btn.reset();
			setError('Password required');
			return;
		}
		try {
			await dispatch(authActions.login(username, password));
			btn.reset();
		} catch (error) {
			btn.reset();
			setError(error);
		}
	};

	useEffect(() => {
		const keyboardDidShowListener = Keyboard.addListener(
			'keyboardDidShow',
			() => {
				setKeyboardVisible(true);
			},
		);
		const keyboardDidHideListener = Keyboard.addListener(
			'keyboardDidHide',
			() => {
				setKeyboardVisible(false);
			},
		);

		return () => {
			keyboardDidHideListener.remove();
			keyboardDidShowListener.remove();
		};
	}, []);

	return (
		<KeyboardAvoidingView
			behavior={Platform.OS == 'ios' ? 'padding' : 'height'}
			style={{ flex: 1 }}
		>
			<TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}>
				<View style={styles.container}>
					<Image
						style={{ position: 'absolute', top: 0, width: 600 }}
						source={require('../images/Image-1.jpg')}
					/>

					{!isKeyboardVisible ? (
						<Image
							style={{ marginBottom: 5 }}
							source={require('../images/logo.png')}
						/>
					) : (
						<View style={{ marginBottom: 100 }} />
					)}

					<Text style={styles.textStyle}>
						Login to continue to the resGrid dashboard
					</Text>
					<TextInput
						style={styles.textInput}
						placeholder='Email id'
						keyboardType='email-address'
						onChangeText={(text) => setUserName(text.trim())}
						placeholderTextColor='gray'
						value={username}
					/>
					<TextInput
						style={styles.textInput}
						placeholder='Password'
						secureTextEntry={true}
						onChangeText={(text) => setPassword(text.trim())}
						placeholderTextColor='gray'
						value={password}
					/>
					<View
						style={[
							{
								width: '80%',
								marginTop: 10,
								alignItems: 'center',
							},
						]}
					>
						<Btn
							label='Login'
							backgroundColor='#4682b4'
							labelStyle={{ color: 'white', fontWeight: 'bold' }}
							foregroundColor='white'
							onPress={() => loginHandler()}
							ref={(ref) => (btn = ref)}
							style={[{ borderRadius: 5 }]}
						/>
					</View>
					{error != '' ? <Text style={{ color: 'red' }}>{error}</Text> : null}
					{access_token ? (
						<TouchableOpacity
							style={{ marginTop: 20 }}
							onPress={() => navigation.navigate('forget')}
						>
							<Text style={{ color: '#4682b4', fontWeight: 'bold' }}>
								Forgot password?
							</Text>
						</TouchableOpacity>
					) : null}
				</View>
			</TouchableWithoutFeedback>
		</KeyboardAvoidingView>
	);
};

const styles = StyleSheet.create({
	textStyle: {
		color: 'gray',
		fontWeight: 'bold',
		marginBottom: 10,
	},
	container: {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center',
		backgroundColor: '#fff',
	},
	textInput: {
		height: 40,
		width: '80%',
		borderColor: 'gray',
		borderWidth: 1,
		margin: 5,
		padding: 10,
	},
});

export default LoginScreen;
