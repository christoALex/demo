import React from 'react';
import { StyleSheet } from 'react-native';
import AnimatedLoader from 'react-native-animated-loader';

export default class Loading extends React.Component {
	constructor(props) {
		super(props);
		this.state = { visible: false };
	}

	render() {
		return (
			<AnimatedLoader
				visible={true}
				overlayColor='rgba(255,255,255,0.75)'
				source={require('../images/load.json')}
				animationStyle={styles.lottie}
				speed={1}
			/>
		);
	}
}

const styles = StyleSheet.create({
	lottie: {
		width: 100,
		height: 100,
	},
});
