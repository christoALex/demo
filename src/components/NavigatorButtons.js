import React, { useState } from 'react';
import {
	FlatList,
	SafeAreaView,
	StatusBar,
	StyleSheet,
	Text,
	TouchableOpacity,
	Button,
} from 'react-native';

const DATA = [
	{
		id: 'Grid',
		title: 'Grid',
	},
	{
		id: 'Battery',
		title: 'Battery',
	},
	{
		id: 'PV Solar',
		title: 'PV Solar',
	},
	{
		id: 'Home Demand',
		title: 'Home\nDemand',
	},
	{
		id: 'Electric Vehicle',
		title: 'Electric\nVehicle',
	},
];

const Item = ({ item, onPress, style }) => (
	<TouchableOpacity style={[styles.item, style]} onPress={onPress}>
		<Text style={styles.title}>{item.title}</Text>
	</TouchableOpacity>
);

const NavigatorButtons = ({ navigation, page }) => {
	const [selectedId, setSelectedId] = useState(null);
	const b = (id) => {
		navigation.navigate(id);
	};

	const renderItem = ({ item }) => {
		const backgroundColor = item.id === page ? '#F18701' : '#FFF';
		return (
			<Item
				item={item}
				onPress={() => b(item.id)}
				style={{ backgroundColor }}
			/>
		);
	};

	return (
		<SafeAreaView style={styles.container}>
			<FlatList
				data={DATA}
				renderItem={renderItem}
				keyExtractor={(item) => item.id}
				extraData={selectedId}
				numColumns={3}
				keyExtractor={(item, index) => index.toString()}
			/>
		</SafeAreaView>
	);
};

const styles = StyleSheet.create({
	container: {
		flex: 1,
		marginTop: StatusBar.currentHeight || 0,
		//marginLeft: '10%',
		marginBottom: '5%',
		alignSelf: 'center',
		position: 'absolute',
		bottom: 0,
	},
	title: {
		textAlign: 'center',
		fontWeight: 'bold',
		fontSize: 13,
	},
	item: {
		alignItems: 'center',
		justifyContent: 'center',
		backgroundColor: '#F18701',
		width: 100,
		padding: 15,
		margin: 10,
		borderColor: '#EDEDED',
		borderWidth: 1,
		borderRadius: 10,
	},
});

export default NavigatorButtons;
