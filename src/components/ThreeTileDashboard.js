import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

const ThreeTileDashboard = (props) => {
	return (
		<View style={{ flex: 1, marginTop: 20 }}>
			<View style={{ alignItems: 'center' }}>
				<View style={styles.dashBoardStyle}>
					<View style={styles.firstRowStyle}>
						<View style={styles.tileStyle}>
							<View style={{ justifyContent: 'center' }}>
								<Text style={styles.headerStyle}>{props.header1}</Text>
							</View>
							<Text style={styles.valueStyle}>{props.value1}</Text>
						</View>
						<View style={{ justifyContent: 'center' }}>
							<View style={styles.verticalLine} />
						</View>
						<View style={styles.tileStyle}>
							<View style={{ justifyContent: 'center' }}>
								<Text style={styles.headerStyle}>{props.header2}</Text>
							</View>
							<Text style={styles.valueStyle}>{props.value2}</Text>
						</View>
					</View>
					<View style={styles.horizontalLine} />
					<View style={{ justifyContent: 'center', alignItems: 'center' }}>
						<View style={styles.tileStyle}>
							<Text style={styles.headerStyle}>{props.header3}</Text>
							<Text style={styles.valueStyle}>{props.value3}</Text>
						</View>
					</View>
				</View>
			</View>
		</View>
	);
};

const styles = StyleSheet.create({
	dashBoardStyle: {
		width: '90%',
		marginVertical: 20,
		borderRadius: 20,
		backgroundColor: 'white',
		elevation: 5,
		shadowColor: '#000',
		shadowOffset: { width: 0, height: 0 },
		shadowOpacity: 0.1,
		shadowRadius: 5,
	},
	firstRowStyle: {
		elevation: 0,
		flexDirection: 'row',
		alignItems: 'center',
	},
	tileStyle: {
		elevation: 0,
		justifyContent: 'center',
		alignItems: 'center',
		width: '50%',
		height: 100,
	},
	headerStyle: {
		textAlign: 'center',
		marginBottom: 15,
		color: '#6d6875',
		fontWeight: 'bold',
	},
	verticalLine: {
		height: 100,
		width: 2,
		marginTop: 10,
		backgroundColor: '#D6D6D6',
	},
	horizontalLine: {
		marginHorizontal: 20,
		width: '90%',
		height: 2,
		backgroundColor: '#D6D6D6',
	},
	valueStyle: {
		fontWeight: 'bold',
		color: '#4682b4',
		fontSize: 18,
		marginBottom: 15,
	},
});

export default ThreeTileDashboard;
