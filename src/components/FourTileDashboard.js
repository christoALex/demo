import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

const FourTileDashboard = (props) => {
	return (
		<View style={{ flex: 1, marginTop: 20 }}>
			<View style={{ elevation: 0, alignItems: 'center' }}>
				<View style={styles.dashBoardStyle}>
					<View style={styles.firstRowStyle}>
						<View style={styles.tileStyle}>
							<View
								style={
									props.icon
										? { ...styles.headerStyle, marginLeft: 20 }
										: styles.headerStyle
								}
							>
								<Text style={styles.headerTextStyle}>{props.header1}</Text>
								{props.icon ? (
									<Icon name='arrow-down' size={20} color='#4682b4' />
								) : null}
							</View>
							<Text style={styles.valueStyle}>{props.value1}</Text>
						</View>
						<View style={{ elevation: 0, justifyContent: 'center' }}>
							<View style={styles.verticalLine} />
						</View>
						<View style={styles.tileStyle}>
							<View
								style={
									props.icon
										? { ...styles.headerStyle, marginLeft: 20 }
										: styles.headerStyle
								}
							>
								<Text style={styles.headerTextStyle}>{props.header2}</Text>
								{props.icon ? (
									<Icon name='arrow-up' size={20} color='#4682b4' />
								) : null}
							</View>
							<Text style={styles.valueStyle}>{props.value2}</Text>
						</View>
					</View>
					<View style={styles.seperatorLines}>
						<View style={styles.lineStyle} />
						<View style={styles.lineStyle} />
					</View>
					<View style={{ elevation: 0, flexDirection: 'row' }}>
						<View style={styles.tileStyle}>
							<View
								style={
									props.icon
										? { ...styles.headerStyle, marginLeft: 20 }
										: styles.headerStyle
								}
							>
								<Text style={styles.headerTextStyle}>{props.header3}</Text>
								{props.icon ? (
									<Icon
										name='arrow-down'
										size={20}
										style={{ marginHorizontal: 5 }}
										color='#4682b4'
									/>
								) : null}
							</View>
							<Text style={styles.valueStyle}>{props.value3}</Text>
						</View>
						<View style={{ elevation: 0, justifyContent: 'center' }}>
							<View style={styles.verticalLine} />
						</View>
						<View style={styles.tileStyle}>
							<View
								style={
									props.icon
										? { ...styles.headerStyle, marginLeft: 20 }
										: styles.headerStyle
								}
							>
								<Text style={styles.headerTextStyle}>{props.header4}</Text>
								{props.icon ? (
									<Icon name='arrow-up' size={20} color='#4682b4' />
								) : null}
							</View>
							<Text style={styles.valueStyle}>{props.value4}</Text>
						</View>
					</View>
				</View>
			</View>
		</View>
	);
};

const styles = StyleSheet.create({
	dashBoardStyle: {
		width: '90%',
		marginVertical: 20,
		borderRadius: 20,
		backgroundColor: 'white',
		elevation: 5,
		shadowColor: '#000',
		shadowOffset: { width: 0, height: 0 },
		shadowOpacity: 0.1,
		shadowRadius: 5,
	},
	firstRowStyle: {
		elevation: 0,
		flexDirection: 'row',
		alignItems: 'center',
	},
	tileStyle: {
		elevation: 0,
		justifyContent: 'center',
		alignItems: 'center',
		width: '50%',
		height: 100,
		marginTop: 10,
	},
	headerStyle: {
		flexDirection: 'row',
		elevation: 0,
		justifyContent: 'center',
	},
	headerTextStyle: {
		textAlign: 'center',
		marginBottom: 15,
		color: '#6d6875',
		fontWeight: 'bold',
	},
	verticalLine: {
		height: 80,
		width: 2,
		backgroundColor: '#D6D6D6',
	},
	valueStyle: {
		fontWeight: 'bold',
		color: '#4682b4',
		fontSize: 18,
		marginBottom: 15,
	},
	seperatorLines: {
		elevation: 0,
		flexDirection: 'row',
		justifyContent: 'space-between',
		alignItems: 'center',
	},
	lineStyle: {
		marginHorizontal: 10,
		width: '40%',
		height: 2,
		backgroundColor: '#D6D6D6',
	},
});

export default FourTileDashboard;
