import { combineReducers } from 'redux';
import AuthReducer from './auth';
import DataRedducer from './data';
import NotificationReducer from './notification';
const rootReducer = combineReducers({
	Auth: AuthReducer,
	GridData: DataRedducer,
	Notification: NotificationReducer,
});

export default rootReducer;
