import { ADD_TOKEN } from '../actions/notification';

const initialState = {
	notificationData: [],
};

const notificationReducer = (state = initialState, action) => {
	switch (action.type) {
		case ADD_TOKEN:
			return {
				notificationData: action.notificationData,
			};
			break;

		default:
			return state;
	}
};

export default notificationReducer;
