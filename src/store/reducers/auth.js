import { LOGIN, LOGOUT } from '../actions/auth';

const initialState = {
	isLoggedIn: false,
	userData: [],
	accessToken: '',
};

const authReducer = (state = initialState, action) => {
	switch (action.type) {
		case LOGIN:
			return {
				isLoggedIn: true,
				userData: action.userData,
				accessToken: action.accessToken,
			};
			break;

		case LOGOUT:
			return {
				isLoggedIn: false,
				userData: [],
				accessToken: action.accessToken,
			};
			break;

		default:
			return state;
	}
};

export default authReducer;
