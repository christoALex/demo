import { GET_DATA } from '../actions/data';

const initialState = {
	resGridData: [],
};

const dataReducer = (state = initialState, action) => {
	switch (action.type) {
		case GET_DATA:
			return {
				resGridData: action.gridData,
			};
			break;

		default:
			return state;
	}
};

export default dataReducer;
