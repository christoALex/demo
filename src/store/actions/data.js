export const GET_DATA = 'GET_DATA';
import { logout } from './auth';
import axios from 'axios';
import { API } from '../../utils/config';
import Toast from 'react-native-simple-toast';

export const getData = (access_token) => {
	return async (dispatch) => {
		try {
			const response = await axios.get(`${API}/plc_data?limit=1`, {
				headers: {
					Authorization: `Bearer ${access_token}`,
				},
			});

			const gridDataValues = response.data;
			if (response.status === 200) {
				dispatch({
					type: GET_DATA,
					gridData: gridDataValues,
				});
			}
		} catch (error) {
			if (error.response.data.statusCode == 401) {
				Toast.show('Access Expired ! Please Login Again');
				setTimeout(function () {
					dispatch(logout(access_token));
				}, 1000);
			}
			let message;
			if (error.response !== undefined) {
				throw error.response.data;
			} else {
				message = error;
			}
			throw message;
		}
	};
};
