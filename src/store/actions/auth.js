export const LOGIN = "LOGIN";
export const LOGOUT = "LOGOUT";

import axios from "axios";
import { API } from "../../utils/config";

export const login = (email, password) => {
  return async (dispatch) => {
    try {
      const response = await axios.post(
        `${API}/auth/login`,
        {
          email: email,
          password: password,
        },
        {
          headers: {
            "Content-Type": "application/json",
          },
        },
      );
      const user = response.data;
      if (response.status === 200) {
        dispatch({
          type: LOGIN,
          userData: user,
          accessToken: user.access_token,
        });
      }
    } catch (error) {
      let message;
      if (error.response !== undefined) {
        throw error.response.data.message;
      } else {
        message = error.message;
      }
      throw message;
    }
  };
};

export const logout = (access_token, playerId) => {
  return async (dispatch) => {
    try {
      const response = await axios.post(
        `${API}/player/mute-notifications`,
        {
          playerId: playerId,
        },
        {
          headers: {
            Authorization: `Bearer ${access_token}`,
          },
        },
      );

      dispatch({
        type: LOGOUT,
        accessToken: access_token,
      });
    } catch (error) {
      dispatch({
        type: LOGOUT,
        accessToken: access_token,
      });
    }
  };
};
