export const ADD_TOKEN = 'ADD_TOKEN';
export const GET_LIST = 'GET_LIST';

import axios from 'axios';
import { API } from '../../utils/config';
import Toast from 'react-native-simple-toast';
import { logout } from './auth';

export const addNotification = (access_token, notificationData) => {
	return async (dispatch) => {
		try {
			const response = await axios.post(
				`${API}/player/save-player`,
				{
					playerId: notificationData.userId,
				},
				{
					headers: {
						Authorization: `Bearer ${access_token}`,
					},
				},
			);
			const success = response.data;
			dispatch({
				type: ADD_TOKEN,
				notificationData: notificationData,
			});
		} catch (error) {
			if (error.response.data.statusCode == 401) {
				Toast.show('Access Expired ! Please Login Again');
				setTimeout(function () {
					dispatch(logout(access_token));
				}, 1000);
			}
			let message;
			if (error.response !== undefined) {
				throw error.response.data;
			} else {
				message = error.message;
			}
			throw message;
		}
	};
};
