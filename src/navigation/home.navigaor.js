import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import HomeScreen from '../screens/HomeScreen';
import Homedemand from '../screens/subscreens/Homedemand';
import Grid from '../screens/subscreens/Grid';
import Pvsolar from '../screens/subscreens/Pvsolar';
import EvScreen from '../screens/subscreens/Ev';
import BatteryScreen from '../screens/subscreens/Battery';

const Stack = createStackNavigator();

export const HomeNavigatorStack = () => {
	return (
		<Stack.Navigator
			screenOptions={{
				headerBackTitleVisible: false,
			}}
		>
			<Stack.Screen name='Home' component={HomeScreen} />
			<Stack.Screen name='Home Demand' component={Homedemand} />
			<Stack.Screen name='Grid' component={Grid} />
			<Stack.Screen name='PV Solar' component={Pvsolar} />
			<Stack.Screen name='Electric Vehicle' component={EvScreen} />
			<Stack.Screen name='Battery' component={BatteryScreen} />
		</Stack.Navigator>
	);
};
