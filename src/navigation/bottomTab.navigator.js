import React from 'react';
import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs';
const BottomTab = createMaterialBottomTabNavigator();
import { HomeNavigatorStack } from './home.navigaor';
import { ProfileNavigatorStack } from './profile.navigator';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import LogoutScreen from '../screens/LogoutScreen';

export const BottomTabNavigator = () => {
	return (
		<BottomTab.Navigator
			activeColor='#3e2465'
			inactiveColor='#0077b6'
			barStyle={{ backgroundColor: '#edf2f4' }}
		>
			<BottomTab.Screen
				name='HomeStack'
				component={HomeNavigatorStack}
				options={{
					tabBarLabel: 'Home',
					tabBarIcon: ({ color }) => (
						<Icon name='home' color={color} size={26} />
					),
				}}
			/>
			<BottomTab.Screen
				name='Profile'
				component={ProfileNavigatorStack}
				options={{
					tabBarLabel: 'Profile',
					tabBarIcon: ({ color }) => (
						<Icon name='face-profile' color={color} size={26} />
					),
				}}
			/>
			<BottomTab.Screen
				name='logout'
				component={LogoutScreen}
				options={{
					tabBarLabel: 'Logout',
					tabBarIcon: ({ color }) => (
						<Icon name='logout' color={color} size={26} />
					),
				}}
			/>
		</BottomTab.Navigator>
	);
};
