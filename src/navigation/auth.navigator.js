import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import LoginScreen from '../screens/LoginScreen';
import ForgetPasswordScreen from '../screens/ForgetPasswordScreen';

const Stack = createStackNavigator();

export const AuthNavigatorStack = () => {
	return (
		<Stack.Navigator>
			<Stack.Screen
				options={{ headerShown: false }}
				name='login'
				component={LoginScreen}
			/>
			<Stack.Screen
				options={{ headerShown: false }}
				name='forget'
				component={ForgetPasswordScreen}
			/>
		</Stack.Navigator>
	);
};
