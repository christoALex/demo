import React, { useState, useEffect } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { useSelector } from 'react-redux';
import { NavigationContainer, DefaultTheme } from '@react-navigation/native';

import NetInfo from '@react-native-community/netinfo';
import Icon from 'react-native-vector-icons/MaterialIcons';

import { AuthNavigatorStack } from './auth.navigator';

import { BottomTabNavigator } from './bottomTab.navigator';

import Loading from '../components/Loading';
import SplashScreen from 'react-native-splash-screen';

const MainNavigator = () => {
	const isAuth = useSelector((state) => state.Auth.isLoggedIn);
	const [isConnected, setIsConnected] = useState(true);
	const [loading, setLoading] = useState(true);

	useEffect(() => {
		const unsubscribe = NetInfo.addEventListener((state) => {
			setIsConnected(state.isConnected);
		});
		SplashScreen.hide();
		setTimeout(function () {
			setLoading(false);
		}, 1000);

		return unsubscribe;
	}, []);

	return (
		<>
			{isConnected ? (
				loading ? (
					<Loading />
				) : (
					<NavigationContainer theme={DefaultTheme}>
						{isAuth ? <BottomTabNavigator /> : <AuthNavigatorStack />}
					</NavigationContainer>
				)
			) : (
				<View style={{ ...styles.screen, ...styles.noConnection }}>
					<Icon name='signal-wifi-off' size={50} style={styles.icon} />
					<Text style={styles.text1}>YOU ARE OFFLINE !</Text>
					<Text style={styles.text2}>PLEASE CONNECT TO INTERNET</Text>
				</View>
			)}
		</>
	);
};

const styles = StyleSheet.create({
	screen: { flex: 1, justifyContent: 'center', alignItems: 'center' },
	noConnection: {
		backgroundColor: 'rgba(0,0,0,0)',
	},
	icon: {
		width: 50,
		height: 50,
	},
	text1: {
		color: 'red',
	},
	text2: {
		color: 'black',
	},
	spinner: {
		backgroundColor: 'black',
	},
});

export default MainNavigator;
