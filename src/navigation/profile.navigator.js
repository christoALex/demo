import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import ProfileScreen from '../screens/ProfileScreen';

const Stack = createStackNavigator();

export const ProfileNavigatorStack = () => {
	return (
		<Stack.Navigator>
			<Stack.Screen name='Profile' component={ProfileScreen} />
		</Stack.Navigator>
	);
};
