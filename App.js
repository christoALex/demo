import 'react-native-gesture-handler';
import React, { useEffect } from 'react';
import MainNavigator from './src/navigation';
import { Provider } from 'react-redux';
import stores from './src/store';
import { PersistGate } from 'redux-persist/integration/react';
const { store, persistor } = stores();
import OneSignal from 'react-native-onesignal';

export default function App() {
	OneSignal.init('392321b2-90a0-4ca6-b9cd-9195e1e1ed88');
	OneSignal.inFocusDisplaying(2);

	function notify(notification) {
		console.log('Notification received: ', notification);
	}

	useEffect(() => {
		OneSignal.addEventListener('received', notify);
		return () => {
			OneSignal.removeEventListener('received', notify);
		};
	}, []);

	return (
		<Provider store={store}>
			<PersistGate loading={null} persistor={persistor}>
				<MainNavigator />
			</PersistGate>
		</Provider>
	);
}
